<div align="center">
  <h1>Microsoft Teams Notifier on GitLab Events</h1>
</div>

> Note: README.md mostly contains the configuration part of the application. For detailed implementation details, please go to [Implementation Details](#implementation-details-and-other-documents). Please read this carefully to configure the app. Also, some of these links are only accessible to people from the [Sprinklr]() organization.

## Brief Description about the Project

This project is a Java (Spring Boot) based application that uses [Bot Framework](https://dev.botframework.com) to send notifications to Microsoft Teams' personal chat whenever a new trigger occurs in a GitLab repository.

## Details about the Application's Implementation

### 1. GitLab Webhook

GitLab webhook is used to trigger the application whenever a new event occurs in the GitLab repository. The application is currently running on my localhost, so I have used ngrok to expose my localhost to the internet. The ngrok URL is used as the webhook URL in GitLab repository settings. The endpoint `/api/gitlab` is used to receive the webhook request from GitLab.

### 2. Events in GitLab

The following events occurred in GitLab are handled in this application:

- Comment on an issue
- Comment on a merge request
- Comment on a commit
- Comment on a snippet
- Accepting a merge request
- Closing a merge request
- Opening a merge request
- Reopening a merge request
- Approving a merge request
- Unapproving a merge request
- Updating a merge request
- Pipeline success
- Pipeline failed
- Job success
- Job failed

### 3. Microsoft Bot Framework

This application uses Microsoft Bot Framework to handle messaging from the [Middleware server](src%2Fmain%2Fjava%2Fcom%2Fsprinklr%2FInternProjectBot%2FController%2FGitlabController.java) to Microsoft Teams.

### 4. Server-side Implementation

The application is implemented using Java (Spring Boot) and is running on my localhost (exposing my localhost to the internet using ngrok). There are two classes of endpoint controllers:
1. GitlabController.java
2. MicrosoftController.java

GitlabController.java is used to receive the webhook request from GitLab from the endpoint `/api/gitlab`, process the request, and send the notifications to Microsoft Teams' personal chat. In an abstract way, this is what it does, but there are many classes under it that perform the operations.

The function `receivePostRequest()`, present in GitlabController.java, does the following jobs:
1. Receives the webhook request from GitLab.
2. Tells the factory class to create the object of the class responsible for processing the request.
3. The created object will process the request data and give the details about what message is to be sent to whom.
4. The details are then passed to the MicrosoftMessageSender interface.
5. The messages are sent by parsing the userEmail and the received data.

## Features of the Application

- Notification through teams for various GitLab events: Comments, Merge Request events, Pipeline events, Job events.
- Notifying people in the discussion thread when a new comment is added.
- Notifying people when someone is mentioned in a comment.
- Links provided in the notification to visit the event.
- Code content on which the comment is made is visible with proper colors displaying the diff.
- Last comment in the discussion thread is visible.
- Reply on the thread from Microsoft Teams.
- Resolve the Discussion thread from Microsoft Teams.
- User Authentication.
- Persisting the conversation state in the database.
- Subscribe and Unsubscribe feature.
- Used Cards to display rich text and HTML content in Teams chat.

## How to Use This Application

### Server-side Configuration

If you want to run this server and deploy the bot with your account, please follow the below steps:

1. If you are trying to run this app on your computer, please look into this website to tunnel the requests: [Ngrok](https://dashboard.ngrok.com/get-started/setup).

2. If you wish to deploy the bot, please follow these steps:

   - Go to [Azure Active Directory](https://portal.azure.com/#home) and search for "Azure Bot" as shown below:
   ![Azure](Screenshots/Screenshot%20from%202023-07-22%2019-37-38.png)

   - Fill the necessary details as shown in the below picture:
   ![Registration](Screenshots/Screenshot%20from%202023-07-22%2019-39-10.png)

   - Fill the API endpoint as `https://<your_host_name>/api/messages`. (Please checkout ngrok link if you are running this application on your localhost). The below picture shows the same:
   ![Endpoint](Screenshots/Screenshot%20from%202023-07-22%2019-39-34.png)

   - Note down the application ID and application secret. The secret will be available when you click on "Manage Password":
   ![Secret](Screenshots/Screenshot%20from%202023-07-22%2019-45-50.png).

   - Paste these in the following fields in [application.properties](src/main/resources/application.properties) file:
     ```properties
     MicrosoftAppId=<your_app_id>
     MicrosoftAppPassword=<your_app_password>
     ```

3. Next, run the application:
   - From the root of this project folder:
   - Build the sample using `mvn package`.
   - Run it by using `java -jar target/bot-proactive-sample.jar`.

4. By default, this application will run on your localhost, on port `3978`. You can change this in the [application.properties](src/main/resources/application.properties) file.
5. Paste the AppId given by Microsoft while you were registering in Azure Bot into the following file [Archive.zip](Archive.zip).
6. That's it. Server-side configuration is done. (Please follow the steps in ngrok accordingly).

### What the User Should Do to Use This App

#### Steps to Be Done in Microsoft Teams

- Download this file and paste it into [Archive.zip](Archive.zip), then follow the below steps from the images:

- Go to the Apps section:
![Screenshot 2023-07-14 at 11.57.51 AM.png](Screenshots%2FScreenshot%202023-07-14%20at%2011.57.51%20AM.png)

- Click on "Manage your apps":
![Screenshot 2023-07-14 at 11.57.57 AM.png](Screenshots%2FScreenshot%202023-07-14%20at%2011.57.57%20AM.png)

- Click on "Upload an app":
![Screenshot 2023-07-14 at 11.58.04 AM.png](Screenshots%2FScreenshot%202023-07-14%20at%2011.58.04%20AM.png)

- Click on "Upload a customized app":
![Screenshot 2023-07-14 at 11.58.10 AM.png](Screenshots%2FScreenshot%202023-07-14%20at%2011.58.10%20AM.png)

Please follow the above steps in Microsoft Teams.

#### Steps to Be Done in GitLab to Receive Notifications

- Click on `Settings/Webhooks` section in the project where you want to receive notifications.
![Screenshots/Screenshot from 2023-07-22 20-27-52.png](Screenshots/Screenshot%20from%202023-07-22%2020-27-52.png)

- Click on "Add new webhook":
![Screenshots/Screenshot from 2023-07-22 20-28-09.png](Screenshots/Screenshot%20from%202023-07-22%2020-28-09.png)

- In the URL section, fill `https://<your_host_name>/api/gitlab/` and tick the checkboxes shown in the below picture:
![Screenshots/Screenshot from 2023-07-22 20-29-09.png](Screenshots/Screenshot%20from%202023-07-22%2020-29-09.png)

- That's it. Configuration is done.

## Implementation Details and Other Documents

The document below contains how I implemented this app:
[Project_Report.docx](https://sprinklr-my.sharepoint.com/personal/nabagata_saha_sprinklr_com/_layouts/15/Doc.aspx?sourcedoc=%7B4051BD3D-4746-4290-8356-6F7490F5E05F%7D&file=Project_Report.docx&action=default&mobileredirect=true&CT=1689316627188&OR=ItemsView)

The link below contains all the implementation details of this application:
[Integration Interns](https://sprinklr-my.sharepoint.com/personal/nabagata_saha_sprinklr_com/_layouts/15/onedrive.aspx?ct=1689313750026&or=Teams%2DHL&ga=1&id=%2Fpersonal%2Fnabagata%5Fsaha%5Fsprinklr%5Fcom%2FDocuments%2FIntegration%20Interns%2FSai%20Akshith%20Arthi&view=0)

